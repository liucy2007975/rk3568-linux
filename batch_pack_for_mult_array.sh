#!/bin/bash
#batch pack image script

#note:	1. <./build.sh init> befor run this script
#	2. check screen dts whether commented and null line number exist


function build_image() {
	if [ ! -d image_back ];then
		mkdir image_back						#bulid directory for store image
	fi

	DTSI_LENGTH=${#DTSI_ARRAY1[*]}

	for((i=0;i<$DTSI_LENGTH;i++));
	do

	echo -e "\n\t====> now, screen board and dtsi is $RP_BOARD:${DTSI_ARRAY1[$i]}\n"
	sed -i -e "${DTS_LINE_NUM1}s/.*$/${DTSI_ARRAY1[$i]}/" ${DTS_PATH1}

	./build.sh			###must need, main compile script
	if [ $? -ne 0 ];then
		echo -e "\n\t====> build.sh fail!\n"
		exit -3
	else
		screen=${DTSI_ARRAY1[$i]}
		screen=${screen#*lcd-} && screen=${screen%.*}
		if [[ $screen =~ "ai" ]];then
			screen=${screen#*ai-}
		fi
		#timeyear=`date +%Y%m%d`
		mytime=`date +%Y%m%d-%H%M%S`
		mv $IMAGE_DIR image_back/update-$RP_BOARD-$RP_SYSTEM-$screen-$mytime.img
		#echo $RP_BOARD-$RP_SYSTEM-$screen-$mytime	#debug infomation
	fi

	done
}



#pro3568 debian
#./build.sh BoardConfig-pro-rk3568-debian.mk
#RP_BOARD=pro-rk3568
#RP_SYSTEM=debian
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=28
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3568.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-mipi0-5.5-720-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-5.5-1080-1920-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-10-1920-1200-rk3568.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-mipi1-8-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080-rk3568.dtsi"'
#)
#build_image



#pro3568 ubuntu
#./build.sh BoardConfig-pro-rk3568-ubuntu.mk
#RP_BOARD=pro-rk3568
#RP_SYSTEM=ubuntu
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=43
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3568.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-hdmi.dtsi"'
#'#include "rp-lcd-mipi0-5-720-1280.dtsi"'
#'#include "rp-lcd-mipi0-5.5-1080-1920.dtsi"'
#'#include "rp-lcd-mipi0-5.5-720-1280.dtsi"'
#'#include "rp-lcd-mipi0-5.5-720-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600.dtsi"'
#'#include "rp-lcd-mipi0-7-1200-1920.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-10-1920-1200.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600.dtsi"'
#'#include "rp-lcd-mipi1-10-800-1280.dtsi"'
#'#include "rp-lcd-mipi1-7-1200-1920.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080.dtsi"'
#)
#build_image




#pro3568 buildroot
#./build.sh BoardConfig-pro-rk3568-buildroot.mk
#RP_BOARD=pro-rk3568
#RP_SYSTEM=buildroot
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=28
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3568.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-mipi0-5.5-720-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-5.5-1080-1920-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-mipi0-10-1920-1200-rk3568.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-mipi1-8-800-1280-rk3568.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600-rk3568.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080-rk3568.dtsi"'
#)
#build_image




#pro3566 buildroot
#./build.sh BoardConfig-pro-rk3566-buildroot.mk
#RP_BOARD=pro-rk3566
#RP_SYSTEM=buildroot
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=30
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3566.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-mipi0-5.5-1080-1920-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-rk3566.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080-rk3566.dtsi"'
#)
#build_image




#pro3566 debian
#./build.sh BoardConfig-pro-rk3566-debian.mk
#RP_BOARD=pro-rk3566
#RP_SYSTEM=debian
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=30
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3566.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-mipi0-5.5-1080-1920-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-rk3566.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-rk3566.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600-rk3566.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080-rk3566.dtsi"'
#)
#build_image



#pro3566 ubuntu
#./build.sh BoardConfig-pro-rk3566-ubuntu.mk
#RP_BOARD=pro-rk3566
#RP_SYSTEM=ubuntu
#IMAGE_DIR=rockdev/update-*.img
#DTS_LINE_NUM1=43
#DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/pro-rk3566.dts
#DTSI_ARRAY1=(
#'#include "rp-lcd-mipi0-5-720-1280.dtsi"'
#'#include "rp-lcd-mipi0-5.5-1080-1920.dtsi"'
#'#include "rp-lcd-mipi0-5.5-720-1280.dtsi"'
#'#include "rp-lcd-mipi0-5.5-720-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-7-1024-600.dtsi"'
#'#include "rp-lcd-mipi0-7-1200-1920.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280.dtsi"'
#'#include "rp-lcd-mipi0-8-800-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280.dtsi"'
#'#include "rp-lcd-mipi0-10-800-1280-v2.dtsi"'
#'#include "rp-lcd-mipi0-10-1920-1200.dtsi"'
#'#include "rp-lcd-mipi1-7-1024-600.dtsi"'
#'#include "rp-lcd-mipi1-10-800-1280.dtsi"'
#'#include "rp-lcd-mipi1-7-1200-1920.dtsi"'
#'#include "rp-lcd-lvds-10-1024-600.dtsi"'
#'#include "rp-lcd-edp-13-1920-1080.dtsi"'
#)
#build_image

#rp3566 ubuntu
./build.sh BoardConfig-rp-rk3566-ubuntu.mk
RP_BOARD=rp-rk3566
RP_SYSTEM=ubuntu
IMAGE_DIR=rockdev/update-*.img
DTS_LINE_NUM1=38
DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/rp-rk3566.dts
DTSI_ARRAY1=(
'#include "rp-lcd-mipi0-5-720-1280.dtsi"'
'#include "rp-lcd-mipi0-5.5-1080-1920.dtsi"'
'#include "rp-lcd-mipi0-5.5-720-1280.dtsi"'
'#include "rp-lcd-mipi0-5.5-720-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-7-1024-600.dtsi"'
'#include "rp-lcd-mipi0-7-1200-1920.dtsi"'
'#include "rp-lcd-mipi0-8-800-1280.dtsi"'
'#include "rp-lcd-mipi0-8-800-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-10-800-1280.dtsi"'
'#include "rp-lcd-mipi0-10-800-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-10-1920-1200.dtsi"'
'#include "rp-lcd-lvds-10-1024-600.dtsi"'
'#include "rp-lcd-edp-13-1920-1080.dtsi"'
)
build_image


#rp3568 ubuntu
./build.sh BoardConfig-rp-rk3568-ubuntu.mk
RP_BOARD=rp-rk3568
RP_SYSTEM=ubuntu
IMAGE_DIR=rockdev/update-*.img
DTS_LINE_NUM1=38
DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/rp-rk3568.dts
DTSI_ARRAY1=(
'#include "rp-lcd-hdmi.dtsi"'
'#include "rp-lcd-mipi0-5-720-1280.dtsi"'
'#include "rp-lcd-mipi0-5.5-1080-1920.dtsi"'
'#include "rp-lcd-mipi0-5.5-720-1280.dtsi"'
'#include "rp-lcd-mipi0-5.5-720-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-7-1024-600.dtsi"'
'#include "rp-lcd-mipi0-7-1200-1920.dtsi"'
'#include "rp-lcd-mipi0-8-800-1280.dtsi"'
'#include "rp-lcd-mipi0-8-800-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-10-800-1280.dtsi"'
'#include "rp-lcd-mipi0-10-800-1280-v2.dtsi"'
'#include "rp-lcd-mipi0-10-1920-1200.dtsi"'
'#include "rp-lcd-lvds-10-1024-600.dtsi"'
'#include "rp-lcd-edp-13-1920-1080.dtsi"'
)
build_image


#rp-box-3568 ubuntu
./build.sh BoardConfig-rp-box-rk3568-ubuntu.mk
RP_BOARD=rp-box-rk3568
RP_SYSTEM=ubuntu
IMAGE_DIR=rockdev/update-*.img
DTS_LINE_NUM1=31
DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/rp-box-rk3568.dts
DTSI_ARRAY1=(
'#include "rp-lcd-hdmi.dtsi"'
'#include "rp-lcd-mipi0-5-720-1280-v2-boxTP.dtsi"'
'#include "rp-lcd-mipi0-7-720-1280.dtsi"'
'#include "rp-lcd-mipi0-10-1200-1920.dtsi"'
'#include "rp-lcd-lvds-10-1024-600-raw.dtsi"'
'#include "rp-lcd-edp-13-1920-1080.dtsi"'
)
build_image



#rp-box-3566 ubuntu
./build.sh BoardConfig-rp-box-rk3566-ubuntu.mk
RP_BOARD=rp-box-rk3566
RP_SYSTEM=ubuntu
IMAGE_DIR=rockdev/update-*.img
DTS_LINE_NUM1=31
DTS_PATH1=kernel/arch/arm64/boot/dts/rockchip/rp-box-rk3566.dts
DTSI_ARRAY1=(
'#include "rp-lcd-hdmi.dtsi"'
'#include "rp-lcd-mipi0-5-720-1280-v2-boxTP.dtsi"'
'#include "rp-lcd-mipi0-7-720-1280.dtsi"'
'#include "rp-lcd-mipi0-10-1200-1920.dtsi"'
'#include "rp-lcd-lvds-10-1024-600-raw.dtsi"'
'#include "rp-lcd-edp-13-1920-1080.dtsi"'
)
build_image
